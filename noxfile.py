import nox

locations = "src", "tests", "noxfile.py"

@nox.session
def tests(session):
    session.run("poetry", "install", external=True)
    session.run("pytest")

@nox.session
def mypy(session):
    args = session.posargs or locations
    session.install("mypy")
    session.run("mypy", *args)
