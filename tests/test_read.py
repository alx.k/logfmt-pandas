"""Test logfmt format support"""

from io import StringIO
from pathlib import Path

import pandas as pd
import pandas._testing as tm

import pytest
from logfmt_pandas import read_logfmt


@pytest.fixture
def logfmt_file_path():
    """logfmt file path"""
    return Path("tests/data/data.log")


@pytest.fixture
def logfmt_file():
    """logfmt file"""
    return StringIO(
        'tag=first foo=1 bar=1.0\ntag="second line" bar=2.0 foo=2\ntag="\\"third line\\"" foo=3 bar=3.0'
    )


class TestLogfmt:
    def test_read_logfmt(self, logfmt_file) -> None:
        expected_df = pd.DataFrame(
            [["first", 1, 1.0], ["second line", 2, 2.0], ['"third line"', 3, 3.0]],
            columns=["tag", "foo", "bar"],
        )

        parsed_df = read_logfmt(logfmt_file)
        tm.assert_frame_equal(parsed_df, expected_df)

    def test_read_logfmt_chunksize(self, logfmt_file) -> None:
        expected_df = pd.DataFrame(
            [["first", 1, 1.0], ["second line", 2, 2.0], ['"third line"', 3, 3.0]],
            columns=["tag", "foo", "bar"],
        )

        logfmt_reader = read_logfmt(logfmt_file, chunksize=2)
        tm.assert_frame_equal(logfmt_reader.read(), expected_df)

    def test_read_logfmt_path(self, logfmt_file_path) -> None:
        expected_df = pd.DataFrame(
            [["first", 1, 1.0], ["second line", 2, 2.0], ['"third line"', 3, 3.0]],
            columns=["tag", "foo", "bar"],
        )

        parsed_df = read_logfmt(logfmt_file_path)
        tm.assert_frame_equal(parsed_df, expected_df)

    def test_read_logfmt_str(self, logfmt_file_path) -> None:
        """Read logfmt file when path is specifild as string"""
        expected_df = pd.DataFrame(
            [["first", 1, 1.0], ["second line", 2, 2.0], ['"third line"', 3, 3.0]],
            columns=["tag", "foo", "bar"],
        )

        parsed_df = read_logfmt(str(logfmt_file_path))
        tm.assert_frame_equal(parsed_df, expected_df)
